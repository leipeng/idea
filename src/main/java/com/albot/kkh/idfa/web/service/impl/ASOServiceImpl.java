package com.albot.kkh.idfa.web.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.albot.kkh.idfa.web.ReportServlet;
import com.albot.kkh.idfa.web.dao.mapper.AsoReportIdfaMapper;
import com.albot.kkh.idfa.web.dao.mapper.YoumiMarketingMapper;
import com.albot.kkh.idfa.web.dao.model.AsoReportIdfa;
import com.albot.kkh.idfa.web.dao.model.YoumiMarketing;
import com.albot.kkh.idfa.web.service.ASOService;

@Service("asoService")
public class ASOServiceImpl implements ASOService {

	@Resource(type = AsoReportIdfaMapper.class)
	private AsoReportIdfaMapper asoReportIdfamapper;

	@Resource(type = YoumiMarketingMapper.class)
	private YoumiMarketingMapper youmiMapper;

	private static Logger reportLogger = LoggerFactory.getLogger("report."
			+ ReportServlet.class.getName());

	@Override
	public int addNewLog(String idfa, String idfaMd5) {
		if (queryInfoByIdfaMd5(idfaMd5) != null) {
			return 0;
		}
		AsoReportIdfa bean = new AsoReportIdfa();
		bean.setId(null);
		bean.setIdfa(idfa);
		bean.setIdfaMd5(idfaMd5);
		return asoReportIdfamapper.insertSelective(bean);
	}

	@Override
	public AsoReportIdfa queryInfoByIdfaMd5(String idfaMd5) {
		return asoReportIdfamapper.selectByMd5(idfaMd5);
	}

	@Override
	public int deleteByIdfaMd5(String idfaMd5) {
		return asoReportIdfamapper.deleteByMd5(idfaMd5);
	}

	@Override
	public int deleteById(AsoReportIdfa bean) {
		bean.setActiveTime(new Date());
		bean.setStatus((byte) 2);
		return asoReportIdfamapper.updateByPrimaryKey(bean);
	}

	@Override
	public Map<String, Object> filterIdfa(String appId, String[] idfas,
			String channelId) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(StringUtils.isEmpty(appId)) {
			result.put("success", false);
			result.put("msg", "appId 为空");
			result.put("error", 4000);
			return result;
		}
		
		if(idfas == null || idfas.length == 0) {
			result.put("success", false);
			result.put("msg", "idfa 为空");
			result.put("error", 6000);
			return result;
		}
		
		if(StringUtils.isEmpty(channelId)) {
			result.put("success", false);
			result.put("msg", "channel 为空");
			result.put("erroe", 6000);
			return result;
		}
		
		List<String> idfaList = Arrays.asList(idfas);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appId", appId);
		params.put("list", idfaList);
		
		List<YoumiMarketing> exists = youmiMapper
				.selectByIdfas(params);
		
		List<String> existIdfa = new ArrayList<String>();
		for (YoumiMarketing bean : exists) {
			existIdfa.add(bean.getIfa());
		}

		for (String idfa : idfaList) {
			if (existIdfa.contains(idfa)) {
				result.put(idfa, "1");
			} else {
				result.put(idfa, "0");
			}
		}

		reportLogger.info("AppId[{}], Channel[{}], idfa[{}]", appId, channelId,
				idfaList.toString());

		return result;
	}

}
