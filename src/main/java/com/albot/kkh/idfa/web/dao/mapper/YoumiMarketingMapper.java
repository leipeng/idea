package com.albot.kkh.idfa.web.dao.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.albot.kkh.idfa.web.dao.model.YoumiMarketing;

public interface YoumiMarketingMapper {
	int deleteByPrimaryKey(Long id);

	int insert(YoumiMarketing record);

	int insertSelective(YoumiMarketing record);

	YoumiMarketing selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(YoumiMarketing record);

	int updateByPrimaryKey(YoumiMarketing record);

	YoumiMarketing selectByIfa(@Param("ifa") String ifa);

	YoumiMarketing selectByMac(@Param("mac") String mac);

	YoumiMarketing selectByMacOrIfa(@Param("appId") String appID,
			@Param("str") String str);

	int deleteByIfaOrMac(@Param("str") String str);

	List<YoumiMarketing> selectByIdfas(Map<String, Object> params);

}