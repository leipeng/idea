package com.albot.kkh.idfa.web.dao.mapper;

import org.apache.ibatis.annotations.Param;

import com.albot.kkh.idfa.web.dao.model.AsoReportIdfa;

public interface AsoReportIdfaMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AsoReportIdfa record);

    int insertSelective(AsoReportIdfa record);

    AsoReportIdfa selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AsoReportIdfa record);

    int updateByPrimaryKey(AsoReportIdfa record);
    
    AsoReportIdfa selectByMd5(@Param("md5") String md5);
    
    int deleteByMd5(@Param("md5") String md5);
}