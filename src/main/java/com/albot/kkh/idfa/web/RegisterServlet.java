package com.albot.kkh.idfa.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.albot.kkh.idfa.web.util.ASOUtils;
import com.albot.kkh.idfa.web.util.MD5Util;

public class RegisterServlet extends HttpServlet {
	
	private static final long serialVersionUID = -1884051391835107861L;
	
	private static Logger logger = LoggerFactory.getLogger("register." + RegisterServlet.class.getName());

	public RegisterServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idfa = request.getParameter("idfa");
		if (idfa != null 
				&& idfa.trim().length() > 0) {
			final HttpClient httpClient = new HttpClient();
			httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(10 * 1000);
			httpClient.getHttpConnectionManager().getParams().setSoTimeout(10 * 1000);
			
			if(ASOUtils.ASO_IDFA_MAP.get(idfa) == null) {
				response.getWriter().write("success");
				return;
			}
			
			String idfaMd5 = idfa;
			
			idfa = ASOUtils.ASO_IDFA_MAP.get(idfa);
			
			ASOUtils.ASO_IDFA_MAP.remove(idfaMd5);
			
			final GetMethod method = new GetMethod(
					"http://www.iyouqian.com/external/callback/1000236/10389/?did=" + idfa + "&ret_type=0");
			String result = "";
			try {
				httpClient.executeMethod(method);
				result = new String(method.getResponseBody(), "UTF-8");
			} catch (Exception e) {
				logger.error("exception", e.getMessage());
			} finally {
				method.releaseConnection();
			}
			if (result.trim().equals("success")) {
				logger.info("idfa[{}]", idfa);
			} else {
				logger.error("Error " + result);
			}
		}
		response.getWriter().write("success");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
