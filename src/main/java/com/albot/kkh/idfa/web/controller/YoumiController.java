package com.albot.kkh.idfa.web.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.albot.kkh.idfa.web.service.YoumiService;

@Controller
public class YoumiController {

	@Resource(name = "youmiService")
	private YoumiService youmiService;

	//@RequestMapping("/youmi/report")
	public void report(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		youmiService.report(request, response);
	}

	//@RequestMapping("/youmi/active")
	public void active(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		youmiService.active(request.getParameter("appId"),
				request.getParameter("mac"), response);
	}

}
