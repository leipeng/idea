package com.albot.kkh.idfa.web.dao.model;

import java.util.Date;

public class AsoReportIdfa {
    private Long id;

    private String idfaMd5;

    private String idfa;

    private Date createTime;

    private Date activeTime;

    private Byte status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdfaMd5() {
        return idfaMd5;
    }

    public void setIdfaMd5(String idfaMd5) {
        this.idfaMd5 = idfaMd5 == null ? null : idfaMd5.trim();
    }

    public String getIdfa() {
        return idfa;
    }

    public void setIdfa(String idfa) {
        this.idfa = idfa == null ? null : idfa.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Date activeTime) {
        this.activeTime = activeTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}