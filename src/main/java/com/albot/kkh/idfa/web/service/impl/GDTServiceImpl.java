package com.albot.kkh.idfa.web.service.impl;

import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import com.albot.kkh.idfa.web.consts.GDTConsts;
import com.albot.kkh.idfa.web.service.GDTService;
import com.albot.kkh.idfa.web.util.MD5Util;

@Service("gdtServiceImpl")
public class GDTServiceImpl implements GDTService {

	private String muid;
	private String appType;
	private long conv_time;
	private String query_string;
	private String signature;
	private String v;
	private String requestUrl;
	private String ip;
	
	private String appId;
	private String signKey;
	private String encryKey;
	private String advertiserId;
	
		
	public GDTServiceImpl() {
	}

	@Override
	public GDTService initParas(String muid, String appType,
			String appId, String signKey, String encryKey, String advertiserId) throws Exception {
		this.muid = muid;
		this.conv_time = System.currentTimeMillis() / 1000;
		this.appType = appType;
		this.appId = appId;
		this.signKey = signKey;
		this.encryKey = encryKey;
		this.advertiserId = advertiserId;
		return this;
	}
	
	@Override
	public GDTService initQueryString() throws Exception {
		StringBuilder queryString = new StringBuilder("");
		queryString
				.append("muid=")
				.append(URLEncoder.encode(this.muid, "utf-8"))
				.append("&conv_time=")
				.append(URLEncoder.encode(String.valueOf(this.conv_time),
						"utf-8")).append("&client_ip=").append(this.ip);

		this.query_string = queryString.toString();

		return this;
	}
	
	
	@Override
	public GDTService signParas() throws Exception {
		StringBuilder page = new StringBuilder("");
		page.append("http://t.gdt.qq.com/conv/app/").append(this.appId).append("/conv?").append(this.query_string);

//		System.out.println("Page");
//		System.out.println(page.toString());
//		System.out.println();

		StringBuilder property = new StringBuilder("");
		property.append(this.signKey).append("&GET&")
				.append(URLEncoder.encode(page.toString(), "utf-8"));

//		System.out.println("Property");
//		System.out.println(property.toString());
//		System.out.println();

		this.signature = MD5Util.encode(property.toString());
		
//		System.out.println("Signature");
//		System.out.println(this.signature);
//		System.out.println();

		return this;
	}

	@Override
	public GDTService encryParas() throws Exception {
		StringBuilder baseData = new StringBuilder("");

		baseData.append(this.query_string).append("&sign=")
				.append(URLEncoder.encode(this.signature, "utf-8"));

//		System.out.println("Basedata:");
//		System.out.println(baseData.toString());
//		System.out.println();

		String simpleXor = MD5Util.simpleXor(baseData.toString(),
				this.encryKey);

		Base64 coder = new Base64();

		this.v = new String(coder.encode(simpleXor.getBytes()), "utf-8");

//		System.out.println("V:");
//		System.out.println(this.v);
//		System.out.println();

		return this;
	}

	@Override
	public String generateRequest() throws Exception {
		StringBuilder reqeust_url = new StringBuilder("");
		reqeust_url.append("http://t.gdt.qq.com/conv/app/")
				.append(this.appId).append("/conv?v=")
				.append(URLEncoder.encode(this.v, "utf-8"))
				.append("&conv_type=MOBILEAPP_ACTIVITE&app_type=")
				.append(URLEncoder.encode(this.appType, "utf-8"))
				.append("&advertiser_id=")
				.append(URLEncoder.encode(this.advertiserId, "utf-8"));
		this.requestUrl = reqeust_url.toString();
		return requestUrl;
	}

	public String getMuid() {
		return muid;
	}

	public void setMuid(String muid) {
		this.muid = muid;
	}

	public long getConv_time() {
		return conv_time;
	}

	public void setConv_time(long conv_time) {
		this.conv_time = conv_time;
	}

	public String getQuery_string() {
		return query_string;
	}

	public void setQuery_string(String query_string) {
		this.query_string = query_string;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	
	@Override
	public void setIp(String ip) {
		this.ip = ip;
	}
}
