package com.albot.kkh.idfa.web.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	
	public static String encode(String str) {
		String md5Str = null;
		
		 try {
			MessageDigest md =  MessageDigest.getInstance("MD5");
			
			byte[] input = str.getBytes();
			
			byte[] buff = md.digest(input);
			
			md5Str = bytesToHex(buff);
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return md5Str;
	}
	
	
	public static String simpleXor(String plaintext, String keyString) {
		StringBuilder result = new StringBuilder("");
		
		int plainLength = plaintext.length();
		int keyLength = keyString.length();
		
		int plain;
		int key;
				
		for(int i = 0 ; i < plainLength ; i++) {
			plain = plaintext.charAt(i);
			key = keyString.charAt(i % keyLength);
			result.append((char) (plain ^ key));		
		}
		
		return result.toString();
	}
	
	public static String bytesToHex(byte[] bytes) {
        StringBuffer md5str = new StringBuffer();
        int digital;
        for (int i = 0; i < bytes.length; i++) {
             digital = bytes[i];
 
            if(digital < 0) {
                digital += 256;
            }
            if(digital < 16){
                md5str.append("0");
            }
            md5str.append(Integer.toHexString(digital));
        }
        return md5str.toString().toLowerCase();
    }
}
