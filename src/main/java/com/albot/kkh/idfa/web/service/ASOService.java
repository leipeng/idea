package com.albot.kkh.idfa.web.service;

import java.util.Map;

import com.albot.kkh.idfa.web.dao.model.AsoReportIdfa;

public interface ASOService {

	public int addNewLog(String idfa, String idfaMd5);

	public AsoReportIdfa queryInfoByIdfaMd5(String idfaMd5);

	public int deleteByIdfaMd5(String idfaMd5);

	public int deleteById(AsoReportIdfa bean);

	public Map<String, Object> filterIdfa(String appId, String[] idfas,
			String channelId);
}
