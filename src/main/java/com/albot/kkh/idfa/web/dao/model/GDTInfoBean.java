package com.albot.kkh.idfa.web.dao.model;

public class GDTInfoBean {

	private String appId;
	private String signKey;
	private String encryKey;
	private String advertiserId;

	public GDTInfoBean() {
	}

	public GDTInfoBean(String appId, String signKey, String encryKey,
			String advertiserId) {
		this.appId = appId;
		this.signKey = signKey;
		this.encryKey = encryKey;
		this.advertiserId = advertiserId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getSignKey() {
		return signKey;
	}

	public void setSignKey(String signKey) {
		this.signKey = signKey;
	}

	public String getEncryKey() {
		return encryKey;
	}

	public void setEncryKey(String encryKey) {
		this.encryKey = encryKey;
	}

	public String getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}

}