package com.albot.kkh.idfa.web.service;

public interface GDTService {
	
	//组合参数
	public GDTService initParas(String muid, String appType,
			String appId, String signKey, String encryKey, String advertiserId) throws Exception;
	
	public GDTService initQueryString() throws Exception;
	
	//参数签名
	public GDTService signParas() throws Exception;
	
	//参数加密
	public GDTService encryParas() throws Exception;
	
	//组装请求
	public String generateRequest() throws Exception;

	
	public String getMuid();

	public long getConv_time();

	public String getQuery_string();

	public String getSignature();

	public String getV();

	public String getRequestUrl();
	
	public void setIp(String ip);

}
