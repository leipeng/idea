package com.albot.kkh.idfa.web.enums;

public enum YoumiStatusEnum {

	VALID((byte)1), DELETE((byte)2);

	private byte value;

	private YoumiStatusEnum(byte val) {
		this.value = val;
	}

	public byte value() {
		return this.value;
	}

}
