package com.albot.kkh.idfa.web.service.impl;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.albot.kkh.idfa.web.dao.mapper.YoumiMarketingMapper;
import com.albot.kkh.idfa.web.dao.model.YoumiMarketing;
import com.albot.kkh.idfa.web.enums.YoumiStatusEnum;
import com.albot.kkh.idfa.web.service.YoumiService;

@Service("youmiService")
public class YoumiServiceImpl implements YoumiService {

	@Resource(type = YoumiMarketingMapper.class)
	private YoumiMarketingMapper youmiMapper;

	private static final Logger reportLogger = LoggerFactory
			.getLogger("youmi.report." + YoumiServiceImpl.class.getName());
	private static final Logger activeLogger = LoggerFactory
			.getLogger("youmi.active." + YoumiServiceImpl.class.getName());

	@Override
	public int addNewInstance(String appId, String mac, String ifa, String callBack) {
		YoumiMarketing bean = new YoumiMarketing();
		bean.setAppId(appId);
		bean.setMac(mac);
		bean.setIfa(ifa);
		bean.setCallBackUrl(callBack);
		bean.setCreateTime(new Date());
		bean.setActiveTime(null);
		bean.setStatus(YoumiStatusEnum.VALID.value());
		return youmiMapper.insertSelective(bean);
	}

	@Override
	public int deleteByIfaOrMac(YoumiMarketing bean) {
		bean.setActiveTime(new Date());
		bean.setStatus(YoumiStatusEnum.DELETE.value());
		return youmiMapper.updateByPrimaryKeySelective(bean);
	}

	@Override
	public int deleteById(Long id) {
		return youmiMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void active(String appId, String str, HttpServletResponse response) throws IOException {
		if (str == null || appId == null) {
			response.getWriter().write("fail");
			activeLogger.error("Param error");
			return;
		}
		YoumiMarketing bean = youmiMapper.selectByMacOrIfa(appId, str);
		if (bean == null || bean.getStatus() == YoumiStatusEnum.DELETE.value()) {
			response.getWriter().write("success");
			return;
		}

		final HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(10 * 1000);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(10 * 1000);

		final GetMethod method = new GetMethod(bean.getCallBackUrl());
		String result = null;
		try {
			httpClient.executeMethod(method);
			result = new String(method.getResponseBody(), "UTF-8");
		} catch (Exception e) {
			activeLogger.error(e.getMessage());
		} finally {
			method.releaseConnection();
		}
		JSONObject obj = new JSONObject(result);

		activeLogger.info(result);

		if (!obj.isNull("c") && obj.getInt("c") == 0) {
			activeLogger.info("AppID[{}], Mac[{}], Ifa[{}], Callback[{}]", bean.getAppId(), bean.getMac(),
					bean.getIfa(), bean.getCallBackUrl());
			deleteByIfaOrMac(bean);
			response.getWriter().write("success");
		} else {
			activeLogger.info(obj.toString());
			response.getWriter().write("fail");
		}
	}

	@Override
	public void report(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String appId = request.getParameter("appid");
		String mac = request.getParameter("mac");
		String ifa = request.getParameter("ifa");
		String callBackUrl = request.getParameter("callback_url");

		if (StringUtils.isEmpty(appId) || (StringUtils.isEmpty(mac) && StringUtils.isEmpty(ifa))
				|| StringUtils.isEmpty(callBackUrl)) {
			response.getWriter().write("fail for parameters");
			reportLogger.error("AppID[{}], Mac[{}], Ifa[{}], Callback[{}]", appId, mac, ifa, callBackUrl);
			return;
		}
		if (youmiMapper.selectByMacOrIfa(appId, ifa) == null) {
			addNewInstance(appId, mac, ifa, callBackUrl);
		}
		reportLogger.info("AppID[{}], Mac[{}], Ifa[{}], Callback[{}]", appId, mac, ifa, callBackUrl);
		response.getWriter().write("success");
	}

	@Override
	public YoumiMarketing queryByIfaOrMac(String str) {
		// TODO Auto-generated method stub
		return null;
	}
}
