package com.albot.kkh.idfa.web.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.albot.kkh.idfa.web.dao.model.YoumiMarketing;

public interface YoumiService {

	public int addNewInstance(String appId, String mac, String ifa,
			String callBack);

	public YoumiMarketing queryByIfaOrMac(String str);

	public int deleteByIfaOrMac(YoumiMarketing bean);

	public int deleteById(Long id);

	public void active(String appId, String str, HttpServletResponse response)
			throws IOException;

	public void report(HttpServletRequest request, HttpServletResponse response)
			throws IOException;

}
