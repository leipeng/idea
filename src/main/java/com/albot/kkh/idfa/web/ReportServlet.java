package com.albot.kkh.idfa.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.albot.kkh.idfa.web.util.ASOUtils;
import com.albot.kkh.idfa.web.util.MD5Util;

public class ReportServlet extends HttpServlet {
	
	private static final long serialVersionUID = -7652805207979910529L;
	
	
	private static Logger logger = LoggerFactory.getLogger("report." + ReportServlet.class.getName());

	public ReportServlet() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String channelId = request.getParameter("c");
		String idfa = request.getParameter("d");
		if (channelId != null && channelId.trim().length() > 0 
				&& idfa != null && idfa.trim().length() > 0) {
			System.out.println("chanel is  " + channelId + " and idfa is "+idfa);
			ASOUtils.ASO_IDFA_MAP.put(MD5Util.encode(idfa), idfa);
			logger.info("channelId[{}] idfa[{}]", channelId, idfa);
		}
		response.getWriter().write("success");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
