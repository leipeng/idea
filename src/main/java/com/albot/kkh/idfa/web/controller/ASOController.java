package com.albot.kkh.idfa.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.albot.kkh.idfa.web.RegisterServlet;
import com.albot.kkh.idfa.web.ReportServlet;
import com.albot.kkh.idfa.web.dao.model.AsoReportIdfa;
import com.albot.kkh.idfa.web.service.ASOService;
import com.albot.kkh.idfa.web.util.MD5Util;

@Controller
public class ASOController {

	private static Logger registerLogger = LoggerFactory.getLogger("register."
			+ RegisterServlet.class.getName());

	private static Logger repostLogger = LoggerFactory.getLogger("report."
			+ ReportServlet.class.getName());

	@Resource(name = "asoService")
	private ASOService asoService;

	@RequestMapping(value = "/RegisterServlet", method = RequestMethod.GET)
	public void register(@RequestParam("idfa") String idfa,
			HttpServletResponse response) throws ServletException, IOException {
		if (idfa != null && idfa.trim().length() > 0) {
			final HttpClient httpClient = new HttpClient();
			httpClient.getHttpConnectionManager().getParams()
					.setConnectionTimeout(10 * 1000);
			httpClient.getHttpConnectionManager().getParams()
					.setSoTimeout(10 * 1000);

			AsoReportIdfa bean = asoService.queryInfoByIdfaMd5(idfa);
			if (bean == null || bean.getStatus() == 2) {
				response.getWriter().write("success");
				return;
			}

			idfa = bean.getIdfa();
			asoService.deleteById(bean);

			final GetMethod method = new GetMethod(
					"http://www.iyouqian.com/external/callback/1000236/10389/?did="
							+ idfa + "&ret_type=0");
			String result = "";
			try {
				httpClient.executeMethod(method);
				result = new String(method.getResponseBody(), "UTF-8");
			} catch (Exception e) {
				registerLogger.error("exception", e.getMessage());
			} finally {
				method.releaseConnection();
			}
			if (result.trim().equals("success")) {
				registerLogger.info("idfa[{}]", idfa);
			} else {
				registerLogger.error("Error " + result);
			}
		}
		response.getWriter().write("success");
	}

	@RequestMapping("/ReportServlet")
	public void report(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String channelId = request.getParameter("c");
		String idfa = request.getParameter("d");
		if (channelId != null && channelId.trim().length() > 0 && idfa != null
				&& idfa.trim().length() > 0) {
			System.out.println("chanel is  " + channelId + " and idfa is "
					+ idfa);
			asoService.addNewLog(idfa, MD5Util.encode(idfa));
			repostLogger.info("channelId[{}] idfa[{}]", channelId, idfa);
		}
		response.getWriter().write("success");
	}

	@RequestMapping(value = "/aso/filterIdfa", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> filterIdfa(
			@RequestParam(value = "appId", required = false) String appId,
			@RequestParam(value = "channel", required = false) String channel,
			@RequestParam(value = "idfa", required = false) String[] idfas) {
		return asoService.filterIdfa(appId, idfas, channel);
	}
}
