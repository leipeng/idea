package com.albot.kkh.idfa.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.albot.kkh.idfa.web.GDTServlet;
import com.albot.kkh.idfa.web.consts.GDTConsts;
import com.albot.kkh.idfa.web.dao.model.GDTInfoBean;
import com.albot.kkh.idfa.web.service.GDTService;
import com.albot.kkh.idfa.web.service.impl.GDTServiceImpl;
import com.albot.kkh.idfa.web.util.HttpUtils;

@Controller
public class GDTController {

	private static final Logger log = LoggerFactory.getLogger("GDT." + GDTServlet.class.getName());
	
	@RequestMapping("/GDTServlet")
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String muid = request.getParameter("muid");
		String appType = request.getParameter("app_type");

		if (muid == null || muid.trim().length() <= 0 || appType == null || appType.trim().length() <= 0) {
			log.info("Params is not applicable");
			response.getWriter().write("success");
			return;
		}

		appType = appType.toUpperCase();
		if (!GDTConsts.GDTPlatforms.contain(appType)) {
			log.info("App type is not applicable");
			response.getWriter().write("success");
			return;
		}

		GDTService service = new GDTServiceImpl();
		List<String> requestUrls = new ArrayList<String>();

		try {
			service.setIp(HttpUtils.getRealIp(request));

			for (GDTInfoBean info : GDTConsts.gdtList) {
				String requestUrl = service.initParas(muid, appType, info.getAppId(), info.getSignKey(),
						info.getEncryKey(), info.getAdvertiserId()).initQueryString().signParas().encryParas()
						.generateRequest();
				requestUrls.add(requestUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(10 * 1000);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(10 * 1000);
		String result = "";
		boolean test = true;
		for (String requestUrl : requestUrls) {
			final GetMethod method = new GetMethod(requestUrl);
			try {
				httpClient.executeMethod(method);
				result = new String(method.getResponseBody(), "UTF-8");
			} catch (Exception e) {
				log.error("exception", e.getMessage());
			} finally {
				method.releaseConnection();
			}
			JSONObject obj = new JSONObject(result);
			
			if (!obj.isNull("ret") && obj.getInt("ret") == 0) {
				log.info("URL[{}]，MUID[{}], appType[{}]", requestUrl, muid, appType);
			} else {
				test = false;
				log.info("URL[{}]，ret[{}]", requestUrl, obj.toString());
			}
		}
		if (test) {
			response.getWriter().write("success");
		} else {
			response.getWriter().write("fail");
		}
	}

}
