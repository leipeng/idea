package com.albot.kkh.idfa.web.consts;

import java.util.ArrayList;
import java.util.List;

import com.albot.kkh.idfa.web.dao.model.GDTInfoBean;

public class GDTConsts {

	// public static final String appid = "908946675";
	//
	// public static final String signKey = "776ec03c20255014";
	//
	// public static final String encryKey = "256d05a1933ece70";
	//
	// public static final String advertiserId = "850566";

	public static List<GDTInfoBean> gdtList = new ArrayList<GDTInfoBean>();

	static {
		gdtList.add(new GDTInfoBean("908946675", "776ec03c20255014",
				"256d05a1933ece70", "850566"));
		gdtList.add(new GDTInfoBean("908946675", "ed336c8ad7223b1f",
				"3a7b333e3c3ade74", "333766"));
		gdtList.add(new GDTInfoBean("908946675", "03ed7f222a6d1ca7",
				"5134e0caab56f6e9", "1149645"));
	}

	public static enum GDTPlatforms {
		ANDROID, IOS;

		public static boolean contain(String key) {
			for (GDTPlatforms temp : GDTPlatforms.values()) {
				if (key.trim().equals(temp.name())) {
					return true;
				}
			}
			return false;
		}
	}
}
